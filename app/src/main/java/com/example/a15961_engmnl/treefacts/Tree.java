package com.example.a15961_engmnl.treefacts;

/**
 * Created by 15961-engmnl on 27/05/2017.
 */

public class Tree {
    public Integer getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(Integer imageLocation) {
        this.imageLocation = imageLocation;
    }

    private Integer imageLocation;
    private String treeName;
    private String treeDescription;



    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }

    public String getTreeDescription() {
        return treeDescription;
    }

    public void setTreeDescription(String treeDescription) {
        this.treeDescription = treeDescription;
    }
}
