package com.example.a15961_engmnl.treefacts;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    ArrayList<Tree> treeArrayList = new ArrayList<>();
    ImageView ivTreeImage;
    TextView tvTreeDescription;
    Button btnNextTree;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        addTrees();
        Random random = new Random(treeArrayList.size());
        Integer randomInt = random.nextInt();
        Log.d(TAG, "onCreate: " +randomInt );

        ivTreeImage.setImageDrawable(getDrawable(treeArrayList.get(0).getImageLocation()));
        tvTreeDescription.setText(treeArrayList.get(0).getTreeDescription());


    }

    private void addTrees() {
        Tree acaciaTree = new Tree();
        acaciaTree.setImageLocation(R.drawable.acaciatree);
        acaciaTree.setTreeDescription("Acacia, commonly known as the wattles or acacias, is a" +
                " large genus of shrubs, lianas and trees in the subfamily Mimosoideae of the pea " +
                "family Fabaceae. Initially it comprised a group of plant species native to Africa" +
                "and Australia, with the first species A. nilotica described by Linnaeus.");
        acaciaTree.setTreeName("Acacia");

        Tree narraTree = new Tree();
        narraTree.setImageLocation(R.drawable.acaciatree);
        narraTree.setTreeDescription("It is a large deciduous tree growing to 30–40 m tall, with a " +
                "trunk up to 2 m diameter. The leaves are 12–22 cm long, pinnate, with 5–11 " +
                "leaflets, the girth is 12–34 m wide. The flowers are produced in panicles 6–13 cm " +
                "long containing a few to numerous flowers; flowering is from February to May in" +
                " the Philippines, Borneo and the Malay peninsula. They are slightly fragrant and " +
                "have yellow or orange-yellow petals.");
        narraTree.setTreeName("Narra");

        treeArrayList.add(acaciaTree);
        treeArrayList.add(narraTree);

    }

    private void init() {
        ivTreeImage = (ImageView) findViewById(R.id.ivTreeImage);
        tvTreeDescription = (TextView) findViewById(R.id.tvTreeDescription);
        btnNextTree = (Button) findViewById(R.id.btnNext);
    }
}
